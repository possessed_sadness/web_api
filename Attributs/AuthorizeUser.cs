﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace web_api.Attributs
{
    public class AuthorizeUser: ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var headers = context.HttpContext.Request.Headers;
            if (headers["hash"] != "Basic YWRtaW46YWRtaW4=")
            {
                context.Result =new UnauthorizedResult();
            }
        }
    }
}
