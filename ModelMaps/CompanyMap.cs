﻿using FluentNHibernate.Mapping;
using web_api.Models;

namespace web_api.ModelMaps
{
    public class CompanyMap : ClassMap<Company>
    {
        public CompanyMap()
        {
            Id(v => v.Id);
            Map(v => v.Name).Unique().Not.Nullable();
            Map(v => v.EstablishmentYear).Not.Nullable();
            HasMany(v => v.Employees).Cascade.DeleteOrphans().Inverse();
        }
    }
}
