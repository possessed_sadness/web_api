﻿using FluentNHibernate.Mapping;
using web_api.Models;

namespace web_api.ModelMaps
{
    public class EmployeeMap : ClassMap<Employee>
    {
        public EmployeeMap()
        {
            Id(v => v.ID);
            Map(v => v.FirstName);
            Map(v => v.LastName);
            Map(v => v.DateOfBirth);
            Map(v => v.JobTitle);
        }
    }
}
