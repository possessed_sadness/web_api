﻿using System;
using Newtonsoft.Json.Linq;

namespace web_api.Models
{
    public class Employee
    {
        public virtual long ID { get; set; }
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual DateTime DateOfBirth { get; set; }
        public virtual JobTitle JobTitle { get; set; }
        public virtual JObject GetJson()
        {
            dynamic o = new JObject();
            o.FirstName = this.FirstName;
            o.LastName = this.LastName;
            o.DateOfBirth = this.DateOfBirth;
            o.JobTitle = this.JobTitle.ToString();
            return o;
        }
    }

    public enum JobTitle
    {
        Administrator,
        Developer,
        Architect,
        Manager
    }
}
