﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System.Reflection;
using System.Configuration;


namespace web_api
{
    public static class DB
    {


        public static ISessionFactory sessionFactory  = CreateSessionFactory();
        #region creat session factory region

        private static ISessionFactory CreateSessionFactory()
        {

            return Fluently.Configure()
                   .Database(CreateDbConfig())      
                   .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()))
                   .ExposeConfiguration(DropCreateSchema) 
                   .BuildSessionFactory();
        }

        private static MySQLConfiguration CreateDbConfig()
        {
            return MySQLConfiguration.
                Standard.
                ConnectionString(c=>c.Is(ConfigurationManager.AppSettings["ConnectionString"]));

        }

        private static void DropCreateSchema(NHibernate.Cfg.Configuration cfg)
        {
            new SchemaExport(cfg)
            .Create(false, true);

        }
        #endregion
    }
}
