﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace web_api.Models
{
    public class Company
    {
        public virtual long Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int EstablishmentYear { get; set; }


        public virtual IList<Employee> Employees { get; set; }
        public virtual JObject GetJson()
        {
            dynamic o = new JObject();
            o.Name = this.Name;
            o.EstablishmentYear = this.EstablishmentYear;
            o.Employees = new JArray(Employees.Select(e => e.GetJson()));
            return o;
        }
        public virtual void Save()
        {
            using (var s = DB.sessionFactory.OpenSession())
            {
                using (var t = s.BeginTransaction())
                {
                    s.Save(this);
                    t.Commit();
                }
                s.Close();
            }
        }


        public static void Delete(long id)
        {
            using (var s = DB.sessionFactory.OpenSession())
            {
                using (var t = s.BeginTransaction())
                {
                    var to_del = s.Get<Company>(id);
                    s.Delete(to_del);
                    t.Commit();
                }
                s.Close();
            }
        }
        public static IList<Company> Search( string Keyword,
                                    DateTime? EmployeeDateOfBirthFrom,
                                    DateTime? EmployeeDateOfBirthTo,
                                    List<string> EmployeeJobTitles)
        {
            var result = new List<Company>();
            using (var s = DB.sessionFactory.OpenSession())
            {
                var q = s.Query<Company>();

                FindCompanyOrEmployeeWhereNameContainKeyword(ref q, Keyword);
                FindCompanyThatHasEmployeeWhoWasBornAfter(ref q, EmployeeDateOfBirthFrom.Value);
                FindCompanyThatHasEmployeeWhoWasBornBefore(ref q, EmployeeDateOfBirthTo.Value);
                FindEmployeeWithTitle(ref q, EmployeeJobTitles);

                result = q.ToList();
                s.Close();
            }
            return result;
            #region query parts
        void FindCompanyOrEmployeeWhereNameContainKeyword(ref IQueryable<Company> query, string keyword)
        {
            if (keyword != null)
                query.Where(e => e.Name.Contains(keyword)
                             || e.Employees.Any(k => k.FirstName.Contains(keyword)
                                                     || k.LastName.Contains(keyword)));
        }

        void FindCompanyThatHasEmployeeWhoWasBornAfter(ref IQueryable<Company> query, DateTime bf)
        {
            if (bf != null)
                query.Where(e => e.Employees.Any(k => k.DateOfBirth.Ticks > bf.Ticks));
        }

        void FindCompanyThatHasEmployeeWhoWasBornBefore(ref IQueryable<Company> query, DateTime bt)
        {
            if (bt != null)
                query.Where(e => e.Employees.Any(k => k.DateOfBirth.Ticks > bt.Ticks));
        }

        void FindEmployeeWithTitle(ref IQueryable<Company> query, List<string> titles)
        {
            if (titles != null)
                query.Where(e => e.Employees.Any(k => titles.Any(t => t == k.JobTitle.ToString())));
        }


            #endregion
        }
    
    }
}
