﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using web_api.Attributs;
using web_api.Models;

namespace web_api.Controllers
{
    [Route("/company")]
    public class CompanyController : Controller
    {
        [HttpPost("creat")]
        [AuthorizeUser]
        public IActionResult CreatCompany([FromBody]Company company)
        {
            if (company==null) NotFound("incorret data format");

            try
            {
                company.Save();
            }
            catch (Exception)
            {
                NotFound("incorret data");
            }
            return Ok(company.Id);
        }

        [HttpPost("search")]
        public IActionResult SearchCompany([FromBody]string Keyword,
                                    [FromBody]DateTime? EmployeeDateOfBirthFrom,
                                    [FromBody]DateTime? EmployeeDateOfBirthTo,
                                    [FromBody]List<string> EmployeeJobTitles)
        {
            var companyList = Company.Search(Keyword, EmployeeDateOfBirthFrom, EmployeeDateOfBirthTo, EmployeeJobTitles);
            return Ok(new JObject("Results",new JArray( companyList.Select(e=>e.GetJson()))));
          
        }

        [HttpPut("update")]
        [AuthorizeUser]
        public IActionResult UpdateCompany([FromBody]Company company)
        {
            if ( company == null) return NotFound();
            company.Save();
            return Ok();
        }

        [HttpDelete("delate/{id}")]
        [AuthorizeUser]
        public IActionResult DelateCompany(long id)
        {
            Company.Delete(id);
            return Ok();
        }
    }
}
